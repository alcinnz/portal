# Portal Search Interface #

This project aims to create a nice user friendly web-based interface to search online triple stores. The ultimate goal is to search ALL RDF using this interface, but it will still be very useful to search at business scale. 

### UI Design ###

Portal is designed to be as familiar as possible whilst still innovating on expressive power, it is designed to emulate the familiarity and power of asking questions. That's why on the surface the homepage is presented similarly to Google or DuckDuckGo, so as not to be intimidating. Beyond that Portal more emulates search in Finder and Mail of Mac OS X Mavericks+ in that each "phrase" is boxed making room for a property selection to show whilst entering a phrase. 

The main search results will show a "force directed graph" visualization of the resulting RDF. "Charts" on the left will visualize numbers and locations, whilst extensions called "lenses" will show on the bottom and help manage the visualization. Other views on the results would be available including the self-describing grid, table, list, and RSS views. 

### How do I get set up? ###

Eventually this will get more involved to facilitate i18n, data querying, and minification but for now just open the index.html file. 

### Contribution guidelines ###

TODO: Write code style guidelines. 

To contribute clone this repository. When you've got something that looks nice, either open a pull request or contact me at alcinnz@eml.cc. 

### Who do I talk to? ###

* Adrian Cochrane, alcinnz@eml.cc