/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

// Requires D3.js
/**
A box containing a sequence of "term" boxes. Tabbing into an empty searchbox 
creates a new term. 
Includes action icons (search and a view selector which triggers search)
*/
define(["data/search", "data/term", "./term"], function(Search, Term, term$) {
    return function search(_, actions, d) {
        var search = new Search()

        var $box = d3.select(_),
            $terms = $box.select('.portal-terms'),
            $actions = $box.select('.portal-actions')
        // Insert actions first so they can comfortably float right. 
        if ($actions.empty())
            $actions = $box.append('div').attr('class', 'portal-actions')
                            .style({float: 'right', cursor: 'pointer'})
        if ($terms.empty())
            $terms = $box.append('div').attr('class', 'portal-terms')
        if ($box.select('.portal-clear').empty())
            $box.append('div').attr('class', 'portal-clear').style('clear', 'both')

        // Show action buttons
        var $action = $actions.selectAll('img').data(actions)
                .on('click', function(d) {d.onclick(search)})
                .attr('src', function(d) {d.src})
        $action.enter().append('img')
                .attr('src', function(d) {return d.src})
                .on('click', function(d) {return d.onclick(search)})

        // Show search terms ...
        term$($terms, search, $box)

        $box.attr('tabindex', 0).on('focus', function() {
            // And create/select them on focus.
            if ($terms.select('.portal-term').empty()) {
                search.push(new Term(["Subject"], ""))
                search.selected = search.terms[0]
                setTimeout(function() {term$($terms, search, $box)}, 1)
            } else setTimeout(function() {
                $terms.select('.portal-term-entry').node().focus()
            }, 1)
        })
    }
})