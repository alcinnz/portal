/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

// Requires D3.js
/**
A box which contains a "property path" label and an editable text field. 
When focused also shows a dropdown for property selection and a 
graphical data entry dependent on the type.

Simply call it passing the element you want to contain the terms & the 
Search object you want represented by those terms. */
define(["./term/properties", "./term/content", "./term/focus", "./term/splitting"], function() {
    var ASPECTS = arguments, PREFIX = 'portal-term'
    return function term($el, search, $searchbox) {
        // Create a basic structure
        var $self = $el.selectAll('div').data(search.terms)

        var $in = $self.enter().append('div').classed(PREFIX, true)
        $in.append('h6').classed(PREFIX+'-property', true)
        $in.append('p').classed(PREFIX+'-entry', true)
        $self.entered = $in

        $self.exit().remove()

        // Apply ASPECTS
        for (var i = 0; i < ASPECTS.length; i++) 
            ASPECTS[i]($self, function() {term($el, search, $searchbox)}, search, $searchbox)
    }
})