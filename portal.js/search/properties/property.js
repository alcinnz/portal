/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
* A simple property selection option. Will get more sophisticated to 
* support property paths. Classed portal-vocab-property.
*
* Called as a D3.js widget.
**/
// Requires D3.js
define(function() {
    return function($self) {
        $self.text(function(d) {return d.label})
        $self.enter().append('div').classed('portal-vocab-property', true)
                .text(function(d) {return d.label})
    }
})