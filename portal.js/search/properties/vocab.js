/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

// Requires D3.js & RDFStore.js
/* Creates a dropdown menu of properties possibly with a visual selector
or autocompletion of the value at the top followed by a searchbox. 

Below that are properties to select either with the mouse or the up and 
down arrow keys. The Shift+/Page Up/Down keys skips between sections. 
Build a path of properties by clicking on a "disclosure" indicator or 
using shift+left/right.

Sections can be collapsed/expanded by clicking on them and the selector 
can only be interacted with via pointing devices (mouse/fingers).

--

CSS wise, the following classes are used:

* portal-vocabs (the dropdown)
* portal-vocab (the individual sections of the dropdown)
* portal-vocab-title (the heading for a section)
* portal-vocab-collapsed (A vocab where only the title should be shown)
* portal-vocab-property (A navigatable entry for a vocab, including search)
* portal-vocab-search (The search box)
* portal-vocab-disclosure (The button on each property for adding to the path)

--

JS wise, just call this with the element you want to be used and an optional 
function for keeping focus on the containing term. 
*/
define(["data/vocabs/load", "./section"], function(vocabs, vocab$) {
    return function($self, term, focus) {
        $self.classed('portal-vocabs', true)
        vocab$($self.selectAll('.portal-vocab').data(vocabs))
    }
})