/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
* A section headed by a div.portal-vocab-title with a div.portal-vocab-property
* For each property in the associated data, all contained in a
* div.portal-vocab-properties. 
*
* Called as D3.js widget, as per the pattern for other modules building up the 
* Portal UI.
**/
// Requires D3.js
define(["./property"], function(property$) {
    return function($self, vocabs) {
        function updateContent($self) {
            $self.select('.portal-vocab-title').text(function(d) {return d.title})
            property$($self.select('.portal-vocab-properties')
                .selectAll('.portal-vocab-property')
                .data(function(d) {return d.properties}))
        }
        updateContent($self)

        var $entered = $self.enter().append('div').classed('portal-vocab', true)
        $entered.append('div').classed('portal-vocab-title', true)
        $entered.append('div').classed('portal-vocab-properties', true)
        updateContent($entered)
    }
})