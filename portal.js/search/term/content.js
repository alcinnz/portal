/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
This file creates an editable field out of .portal-term-entry, which is
saved to the object property of that term.
*/
define([], function() {
    function get(key) {
        return function(obj) {return obj[key]}
    }
    return function($self, $$, search) {
        function updateContent($self) {
            $self.select('.portal-term-entry').text(get('object'))
            $self.select('.portal-term-property').text(function(d) {
                return d.predicates[0]
            })
        }
        updateContent($self)

        $self.entered.select('.portal-term-entry')
                .attr('contenteditable', true)
                .on('keydown.content', function(d) {
                    d.object = d3.select(this).text() // In case others reload display
                }).on('blur.content', function(d, i) {
                    if (!d.object.length) {
                        search.pop(i)
                        setTimeout($$, 0)
                    }
                })
        updateContent($self.entered)
    }
})