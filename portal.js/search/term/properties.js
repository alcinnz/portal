/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/** Shows a property dropdown for focused terms. */
define(["util/domReady", "util/findPos", "../properties/vocab"], function(_, findPos, vocab$) {
    var $container;
    _(function() {
        $container = d3.select('body').append('div').classed('portal-properties-outer', true)
                            .style('display', 'none')
        vocab$($container.append('div'))
    })
    return function($self) {
        $self.entered.select('.portal-term-entry').on('focus.properties', function() {
            // TODO Abstract away the positioning to aid extensions and
            // (for browser extensions embedding this) substitution
            var $sel = d3.select(this.parentNode),
                pos = findPos($sel.node()),
                height = parseInt($sel.style('height'))
            height += parseInt($sel.style('padding-top')) + parseInt($sel.style('padding-bottom'))
            height += parseInt($sel.style('margin-top')) + parseInt($self.style('margin-bottom'))
            $container.style({
                position: 'absolute',
                left: pos.x + 'px',
                top: (pos.y + height) + 'px',
                display: 'block'
            })
        }).on('blur.properties', function() {
            $container.style('display', 'none')
        })
    }
})