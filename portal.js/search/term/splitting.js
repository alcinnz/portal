/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/** This file ensures that tabbing happens splits the term at the caret
& focuses the new term. If the term is empty, focuses the next one.

Manually implements tabbing to circumvent glitches with D3's element 
caching. This aspect's call's to focus is why $$ is often wrapped with 
a setTimeout call.*/
define(['util/caretPos'], function(caret) {
    return function($self, $$, search) {
        function applyFocus(d, i) {
            if (d == search.selected) this.focus()
        }
        $self.select('.portal-term-entry').each(applyFocus)
        $self.entered.select('.portal-term-entry').each(applyFocus)
                .on('keydown.focus', function(d, i) {
                    if (d3.event.keyCode == 9 && !d3.event.shiftKey) {
                        if (d.object.length) search.split(i, caret(this))

                        var next = i + 1
                        if (next < search.terms.length) {
                            search.selected = search.terms[next]
                            d3.event.preventDefault()
                        } 
                        // Delete can be slow, this is just as convenient,
                        // And has the same effect for us (though there is a difference). 
                        else search.selected = undefined; 

                        setTimeout($$, 10)
                    }
                })
    }
})