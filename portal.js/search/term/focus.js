/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/** 
This file applies the portal-term-focus class to the currently focused term,
and ensures users don't have the frustration that comes with tabbing to the
searchbox.*/
define([], function() {
    var CLASS = 'portal-term-selected'
    return function($self, _recurse, _search, $searchbox) {
        var $entry = $self.entered.select('.portal-term-entry').on('focus.focus', function() {
            d3.selectAll('.'+CLASS).classed(CLASS, false)
            d3.select(this.parentNode).classed(CLASS, true)
        }).on('blur.focus', function() {
            d3.select(this.parentNode).classed(CLASS, false)
        })

        if ($searchbox) $entry.on('keydown.focus', function() {
            if (d3.event.keyCode == 9) {
                var tabindex = $searchbox.attr('tabindex')
                $searchbox.attr('tabindex', -1)
                setTimeout(function() {$searchbox.attr('tabindex', tabindex)}, 1)
            }
        })

        $self.entered.on('click.focus', function() {
            d3.select(this).select('.portal-term-entry').node().focus()
        })
    }
})