/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
Initializes the core UI components of Portal.js from predetermined IDs, with this no JavaScript is needed to embed Portal.

Simply apply the 'portal-search' ID to the element you want to use as your searchbox and 'portal-bookmarks' to the element 
you want users to drag bookmarks into. Eventually a logo and results section will also be accessible via IDs. 

Not tested, but you should be able to include fallback content in these elements. 
*/
require(['search/searchbox', 'bookmarks/bookmarks', 'util/domReady'], function(searchbox, bookmarks, _){
    _(function() {
        bookmarks('#portal-bookmarks')
        searchbox('#portal-search', [{
                        onclick: function() {
                            alert("TODO: Implement.")
                        },
                        src: 'img/search.png'
                        },
                        {
                        onclick: function() {},
                        src: 'img/down.png'
                        }])
    })
})