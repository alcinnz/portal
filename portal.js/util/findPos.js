// This code originated from http://www.quirksmode.org/js/findpos.html
// Placed in a Require.js module & semicolons removed for Portal.
// Also there was an unmatched open curly bracket which is removed here
// And I return an object instead of a 2 element array. 

// Very simple algorithm but they knew the API better than me (Adrian Cochrane). 

define(function() {
    return function(obj) {
        var curleft = curtop = 0
        if (obj.offsetParent) do {
            curleft += obj.offsetLeft
            curtop += obj.offsetTop
        } while (obj = obj.offsetParent)
        return {x: curleft, y: curtop}
    }
})