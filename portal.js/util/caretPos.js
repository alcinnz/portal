// This code originated from http://stackoverflow.com/questions/3972014/get-caret-position-in-contenteditable-div?rq=1#answer-3976125
// Placed within a Require.js module & semicolons removed for Portal.js;

/**
This function takes an element and returns an integer index into that element.
*/
define(function() {
    return function getCaretPosition(editableDiv) {
        var caretPos = 0, containerEl = null, sel, range
        if (window.getSelection) {
            sel = window.getSelection()
            if (sel.rangeCount) {
                range = sel.getRangeAt(0)
                if (range.commonAncestorContainer.parentNode == editableDiv) {
                    caretPos = range.endOffset
                }
            }
        } else if (document.selection && document.selection.createRange) {
            range = document.selection.createRange()
            if (range.parentElement() == editableDiv) {
                var tempEl = document.createElement("span")
                editableDiv.insertBefore(tempEl, editableDiv.firstChild)
                var tempRange = range.duplicate()
                tempRange.moveToElementText(tempEl)
                tempRange.setEndPoint("EndToEnd", range)
                caretPos = tempRange.text.length
            }
        }
        return caretPos
    }
})