/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
Call with a selector for the element you want users to be able to drag bookmarks into. 

Requires HTML5 Drag and Drop and localstorage. 

Included with Portal so that the internet search engine can share useful searches and to 
store links to the user's choice of services. That is provide quick access to services 
while not biasing Portal. 
*/
define(function() {
    var data = JSON.parse('bookmarks' in localStorage ? localStorage['bookmarks'] : '[]'),
        DELETE = {}

    function get(prop) {
        return function(d) {return d[prop]}
    }
    function cancel() {
        if (d3.event.stopPropagation) d3.event.stopPropagation()
        if (d3.event.preventDefault) d3.event.preventDefault()
        return false
    }
    function save(_) {
        localStorage['bookmarks'] = JSON.stringify(data)
    }

    return function bookmarks(_) {
        function acceptDrop(d, i) {
            var drag = d3.event.dataTransfer,
                mozURLs = drag.getData("text/x-moz-url").split("\n"), // Firefox convenience
                uris = drag.getData("text/uri-list").split("\n"),
                toAdd = []

            for (var j = 0; j < uris.length; j++) {
                var label;
                if (mozURLs) label = mozURLs[j*2+1]

                // Clear if already in bookmarks
                for (var k = 0; k < data.length; k++) if (data[k].href == uris[j]) {
                    data.splice(k, 1);
                    break;
                }

                // And add to new position.
                toAdd[j] = {
                    href: uris[j],
                    label: label ? label : prompt("Bookmark name?", uris[j])
                }
            }
            if (i != DELETE) 
                data.splice.apply(data, [i, 0].concat(toAdd))
            save(); bookmarks(_)

            return cancel()
        }

        var $ = d3.select(_),
            $bookmarks = $.selectAll('li').data(data)

        var $bm_enter = $bookmarks.enter().append('li')
        $bm_enter.append('span').attr('class', 'bm-target').text('|')
        $bm_enter.append('a')
        $bookmarks.exit().remove()

        if (!$bm_enter.empty()) bookmarks(_)// Ensure $bookmarks is the full collection
        var $sep = $bookmarks.select('span').on({
                dragover: cancel,
                dragenter: cancel,
                drop: acceptDrop
            }),
            $links = $bookmarks.select('a')
                        .text(get('label'))
                        .attr('href', get('href'))

        $.select('#bm-add').remove()
        $add = $.append('li').attr({'class': 'bm-target', id:'bm-add'}).text('|')
        $add.on({
            dragover: cancel, dragenter: cancel,
            drop: function() {return acceptDrop(undefined, data.length)}
        })

        // Dragging elsewhere removes bookmarks, not working. Reimplement.
        d3.select('body').on({
            dragover: cancel, dragenter: cancel,
            drop: function() {return acceptDrop(undefined, DELETE)}
        })
    }
})