/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
Represents a URL reference, long form, in a query.

Allows different encodings between hashstring and SPARQL
*/
define(function() {
    function URL(url) {
        this.url = url
    }
    URL.prototype = {
        toFullURL: function() {
            return this.url
        },
        encode: function(encoder, data) {
            encoder.encodeURL(this, data)
        },
        toString: function() {return this.url}
    }
    return URL
})