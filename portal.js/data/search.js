/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
Class Search
+ terms -- An Array of Terms which makes up this Search. Initialized from initializer arguments. 
+ view -- A string indicating the name of the view to use for visualizing the results from this search.
+ namespaces() -- Returns an object mapping all namespaces used in the search to URLs.
+ encode(encoder) -- Visitor method to encode this Search.
* DEFAULT_VIEW -- The default value for view. 

Represents a search query for use in the interface. 
*/
define(["./term"], function(Term) {
    function Search() {
        this.terms = [].slice.call(arguments)
        this.view = Search.DEFAULT_VIEW
    }
    Search.prototype = {
        namespaces: function() {
            // TODO Should probably cache this
            var namespaces = {}
            for (var i = 0; i < this.terms.length; i++) {
                var termns = this.terms[i].namespaces()
                for (var namespace in termns) 
                    namespaces[namespace] = termns[namespace]
            }
            return namespaces
        },
        encode: function(encoder, data) {
            return encoder.encodeSearch(this, data)
        },
        push: function(term) {
            this.terms.push(term)
        },
        pop: function(i) {
            this.terms.splice(i, 1)
        },
        split: function(i, j) {
            var that = this.terms[i],
                newTerm = new Term(that.predicates, that.object.substring(j))

            that.object = that.object.substring(0, j)
            this.terms.splice(i+1, 0, newTerm)
        }
    }
    Search.DEFAULT_VIEW = ""
    return Search
})