/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
Convenience class representing a variable. Basically acts like a string with a fixed
? prefix. Mostly to make code nicer. 
*/
define(function() {
    function Var(name) {
        this.text = "?" + name
    }
    Var.prototype.encode = Var.prototype.toString = function(/*encoder, data*/) {
        return this.text
    }
    
})