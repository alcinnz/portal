/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
* Returns a JSON array of vocabulary objects. Each vocabulary object
* Has a title (type String) and properties (an array of property objects)
* fields, where property objects have label, comment, range, and domain 
* properties.
*
* Initially this is hard coded, but will be loaded out of configuration
* (user or web developer) later. 
**/
// Auto-generated from vocab-generator/index.html. OWL ontology removed. 
define(function() {
    return [ { "properties" : [ { "comment" : "The object of the subject RDF statement.",
          "label" : "Object "
        },
        { "comment" : "The subject is an instance of a class.",
          "label" : "Type "
        },
        { "comment" : "The subject of the subject RDF statement.",
          "label" : "Subject "
        },
        { "comment" : "The predicate of the subject RDF statement.",
          "label" : "Predicate "
        },
        { "comment" : "Idiomatic property used for structured values.",
          "label" : "Value "
        },
        { "comment" : "The first item in the subject RDF list.",
          "label" : "First "
        },
        { "comment" : "The rest of the subject RDF list after the first item.",
          "label" : "Rest "
        }
      ],
    "title" : "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  },
  { "properties" : [ { "comment" : "A summary of the resource.",
          "label" : "Abstract "
        },
        { "comment" : "A related resource of which the described resource is a version, edition, or adaptation.",
          "label" : "Is Version Of "
        },
        { "comment" : "An entity primarily responsible for making the resource.",
          "label" : "Creator "
        },
        { "comment" : "Date on which the resource was changed.",
          "label" : "Date Modified "
        },
        { "comment" : "An entity responsible for making the resource available.",
          "label" : "Publisher "
        },
        { "comment" : "A name given to the resource.",
          "label" : "Title "
        },
        { "comment" : "An account of the resource.",
          "label" : "Description "
        },
        { "comment" : "A related resource that is a version, edition, or adaptation of the described resource.",
          "label" : "Has Version "
        },
        { "comment" : "Date of formal issuance (e.g., publication) of the resource.",
          "label" : "Date Issued "
        },
        { "comment" : "Information about who can access the resource or an indication of its security status.",
          "label" : "Access Rights "
        },
        { "comment" : "Information about rights held in and over the resource.",
          "label" : "Rights "
        },
        { "comment" : "The method by which items are added to a collection.",
          "label" : "Accrual Method "
        },
        { "comment" : "The frequency with which items are added to a collection.",
          "label" : "Accrual Periodicity "
        },
        { "comment" : "The policy governing the addition of items to a collection.",
          "label" : "Accrual Policy "
        },
        { "comment" : "An alternative name for the resource.",
          "label" : "Alternative Title "
        },
        { "comment" : "A class of entity for whom the resource is intended or useful.",
          "label" : "Audience "
        },
        { "comment" : "Date (often a range) that the resource became or will become available.",
          "label" : "Date Available "
        },
        { "comment" : "A point or period of time associated with an event in the lifecycle of the resource.",
          "label" : "Date "
        },
        { "comment" : "A bibliographic reference for the resource.",
          "label" : "Bibliographic Citation "
        },
        { "comment" : "An unambiguous reference to the resource within a given context.",
          "label" : "Identifier "
        },
        { "comment" : "An established standard to which the described resource conforms.",
          "label" : "Conforms To "
        },
        { "comment" : "A related resource.",
          "label" : "Relation "
        },
        { "comment" : "An entity responsible for making contributions to the resource.",
          "label" : "Contributor "
        },
        { "comment" : "The spatial or temporal topic of the resource, the spatial applicability of the resource, or the jurisdiction under which the resource is relevant.",
          "label" : "Coverage "
        },
        { "comment" : "Date of creation of the resource.",
          "label" : "Date Created "
        },
        { "comment" : "Date of acceptance of the resource.",
          "label" : "Date Accepted "
        },
        { "comment" : "Date of copyright.",
          "label" : "Date Copyrighted "
        },
        { "comment" : "Date of submission of the resource.",
          "label" : "Date Submitted "
        },
        { "comment" : "A class of entity, defined in terms of progression through an educational or training context, for which the described resource is intended.",
          "label" : "Audience Education Level "
        },
        { "comment" : "The size or duration of the resource.",
          "label" : "Extent "
        },
        { "comment" : "The file format, physical medium, or dimensions of the resource.",
          "label" : "Format "
        },
        { "comment" : "A related resource that is substantially the same as the pre-existing described resource, but in another format.",
          "label" : "Has Format "
        },
        { "comment" : "A related resource that is included either physically or logically in the described resource.",
          "label" : "Has Part "
        },
        { "comment" : "A process, used to engender knowledge, attitudes and skills, that the described resource is designed to support.",
          "label" : "Instructional Method "
        },
        { "comment" : "A related resource that is substantially the same as the described resource, but in another format.",
          "label" : "Is Format Of "
        },
        { "comment" : "A related resource in which the described resource is physically or logically included.",
          "label" : "Is Part Of "
        },
        { "comment" : "A related resource that references, cites, or otherwise points to the described resource.",
          "label" : "Is Referenced By "
        },
        { "comment" : "A related resource that supplants, displaces, or supersedes the described resource.",
          "label" : "Is Replaced By "
        },
        { "comment" : "A related resource that requires the described resource to support its function, delivery, or coherence.",
          "label" : "Is Required By "
        },
        { "comment" : "A language of the resource.",
          "label" : "Language "
        },
        { "comment" : "A legal document giving official permission to do something with the resource.",
          "label" : "License "
        },
        { "comment" : "An entity that mediates access to the resource and for whom the resource is intended or useful.",
          "label" : "Mediator "
        },
        { "comment" : "The material or physical carrier of the resource.",
          "label" : "Medium "
        },
        { "comment" : "A statement of any changes in ownership and custody of the resource since its creation that are significant for its authenticity, integrity, and interpretation.",
          "label" : "Provenance "
        },
        { "comment" : "A related resource that is referenced, cited, or otherwise pointed to by the described resource.",
          "label" : "References "
        },
        { "comment" : "A related resource that is supplanted, displaced, or superseded by the described resource.",
          "label" : "Replaces "
        },
        { "comment" : "A related resource that is required by the described resource to support its function, delivery, or coherence.",
          "label" : "Requires "
        },
        { "comment" : "A person or organization owning or managing rights over the resource.",
          "label" : "Rights Holder "
        },
        { "comment" : "A related resource from which the described resource is derived.",
          "label" : "Source "
        },
        { "comment" : "Spatial characteristics of the resource.",
          "label" : "Spatial Coverage "
        },
        { "comment" : "The topic of the resource.",
          "label" : "Subject "
        },
        { "comment" : "A list of subunits of the resource.",
          "label" : "Table Of Contents "
        },
        { "comment" : "Temporal characteristics of the resource.",
          "label" : "Temporal Coverage "
        },
        { "comment" : "The nature or genre of the resource.",
          "label" : "Type "
        },
        { "comment" : "Date (often a range) of validity of a resource.",
          "label" : "Date Valid "
        }
      ],
    "title" : "http://purl.org/dc/terms/"
  },
  { "properties" : [ { "comment" : "The summary text of a news Asset",
          "label" : "Asset Summary "
        },
        { "comment" : "Property that associates one asset with another asset",
          "label" : "Associated Asset "
        },
        { "comment" : "Property that associates a thumbnail image with an asset",
          "label" : "Has Thumbnail "
        },
        { "comment" : "Relates an asset to an asset that it is derived from",
          "label" : "Derived From "
        },
        { "comment" : "Property that associates a Text transcript with a Video",
          "label" : "Has Transcript "
        },
        { "comment" : "The title of a news Asset",
          "label" : "Asset Title "
        },
        { "comment" : "The byline of a news Asset",
          "label" : "Asset Byline "
        },
        { "comment" : "The date and time a news Asset was created",
          "label" : "Asset Created Date Time "
        },
        { "comment" : "The date and time a news Asset was modified",
          "label" : "Asset Modified Date Time "
        },
        { "comment" : "The date and time a news Asset was published",
          "label" : "Asset Published Date Time "
        },
        { "comment" : "The date and time a news Asset is embargoedUntil",
          "label" : "Embargoed Until "
        }
      ],
    "title" : "http://data.press.net/ontology/asset/"
  },
  { "properties" : [ { "comment" : "String property that indicates the long name of a Person or Organization via foaf:Agent. Also infers Stuff label via pns:label. For example 'Manchester United F.C.'",
          "label" : "Long Name "
        },
        { "comment" : "String property that indicates the definitive label of a Stuff instance. This might be the full name of a Person, Organization or something Intangible",
          "label" : "Label "
        },
        { "comment" : "String property that indicates the definitive description of a Stuff instance.",
          "label" : "Comment "
        },
        { "comment" : "Property that notably associates stuff together, for example Karl Lagerfeld is notably associated with Fashion",
          "label" : "Notably Associated With "
        },
        { "comment" : "String property that indicates an alias of a Stuff instance. For example 'Television' might have an alias of 'TV'.",
          "label" : "Alias "
        },
        { "comment" : "String property that indicates the definitive full name of a Person or Organization via foaf:Agent. Also infers Stuff label via pns:label",
          "label" : "Name "
        },
        { "comment" : "String property that indicates the short name of a Person or Organization via foaf:Agent. Also infers Stuff label via pns:label. For example 'Man Utd'",
          "label" : "Short Name "
        },
        { "comment" : "Property that associates assets directly with domain entities, e.g. official biography, corporate logo",
          "label" : "Has Asset "
        },
        { "comment" : "Property that associates images with domain entities, e.g. official photograph, corporate logo",
          "label" : "Has Image "
        },
        { "comment" : "Property of a Person. A person's place of birth",
          "label" : "A Person's Place Of Birth "
        },
        { "comment" : "Property of a Person. A person's date of birth",
          "label" : "A Person's Date Of Birth "
        },
        { "comment" : "Property of a Person. A person's date of death",
          "label" : "A Person's Date Of Death "
        }
      ],
    "title" : "http://data.press.net/ontology/stuff/"
  },
  { "properties" : [ { "comment" : "Property that associates Classifications with Classifiables. e.g. <MyAsset> isClassifiedBy <SomeClassification>",
          "label" : "Is Classified By "
        } ],
    "title" : "http://data.press.net/ontology/classification/"
  },
  { "properties" : [ { "comment" : "Property for expressing an event title. Datatype is String",
          "label" : "An Event Title "
        },
        { "comment" : "Property for expressing an event summary. Datatype is String",
          "label" : "An Event Summary "
        },
        { "comment" : "Property for expressing the theme of an event, its range is pns:Intangible",
          "label" : "Theme "
        }
      ],
    "title" : "http://data.press.net/ontology/event/"
  },
  { "properties" : [ { "comment" : "Associates an Identifier to an Identifiable thing",
          "label" : "Has Identifier "
        },
        { "comment" : "Property indicating the literal value of the Identifier",
          "label" : "The Value Of The Identifier "
        },
        { "comment" : "Property defining the containing authority of an Identifier",
          "label" : "Authority "
        }
      ],
    "title" : "http://data.press.net/ontology/identifier/"
  },
  { "properties" : [ { "comment" : "Property that associates Tags with Taggables: Taggable X isTaggedWith Tag Y",
          "label" : "Is Tagged With "
        },
        { "comment" : "Property that associates Tags with Taggables, refines pnt:isTaggedWith: Taggable X about Tag Y. pnt:about is a stronger relationship that pnt:mentions",
          "label" : "About "
        },
        { "comment" : "Property that associates Tags with Taggables, refines pnt:isTaggedWith: Taggable X mentions Tag Y. pnt:mentions is a weaker relationship that pnt:about",
          "label" : "Mentions "
        }
      ],
    "title" : "http://data.press.net/ontology/tag/"
  }
]
})