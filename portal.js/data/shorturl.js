/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
This represents a URL reference, short form, in a query.

Statically compiles to namespace:suburl. */
define(function() {
    function URL(ns, nsurl, suburl) {
        this.namespace = {localname: ns, url: nsurl}
        this.nsurl = nsurl
        this.suburl = suburl
    }
    URL.prototype = {
        toFullURL: function() {
            return this.namespace.url + this.suburl
        },
        encode: function(/*encoder, data*/) {
            return this.namespace.localname + ":" + this.suburl
        },
        toString: function() {
            return this.encode()
        }
    }
    return URL
})