/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
Class Term:
+ predicates -- The sequence of predicates to follow from an unknown value to @object.
+ object -- The value to look for. 
+ range() -- The possible datatypes for the "subject" of this term. Typically only one possible datatype.
    If this is empty, any type is possible.
+ encode(encoder) -- Visitor method to encode this search. 

Represents an individual search term, or triplet pattern, in a Search.
The range method allows us to better constrain the possible properties for a user to select from whilst
providing a sequence of properties allows users to express themselves more. 
*/
define(function() {
    function Term(predicates, object) {
        this.predicates = predicates || []
        this.object = object || ""
    }
    Term.prototype = {
        range: function() {
            if (this.predicates.length)
                return this.predicates[this.predicates.length - 1].range
            else return []
        },
        namespaces: function() {
            var namespaces = {}
            for (var i = 0; i < this.predicates.length; i++) {
                var ns = this.predicates[i].namespace
                if (ns) namespaces[ns.localname] = ns.url
            }
            if (this.object.namespace) 
                namespaces[this.object.namespace.localname] = this.object.namespace.url
            return namespaces
        },
        encode: function(encoder, data) {return encoder.encodeTerm(this, data)}
    }
    return Term
})