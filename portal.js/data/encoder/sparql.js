/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/** 
Implements an encoder to translate a Portal search into a SPARQL query. 

Works one way and allows Portal to consult a standard triplestore.

May be extended with methods to translate a specified property, where 
that property is specified by URL in the name of the method. These will
be refered to as "computed properties". 
This is provided to allow filter functions to sit nicely alongside RDF 
properties instead of bloating the UI to provide for them.

---

Encoder takes additional properties which can be modified by computed properties
(unless otherwise specified, defaults to []):

* select  -  An array of values to select. Defaults to ["?this"]
* describe  -  The variable name to describe (without the ? prefix). If specified 
                overwrites select.
* body  -  Additional triplet patterns for the query body. 
* filters  -  SPARQL filter expressions to limit the query. 
* modifiers  -  SPARQL clauses to follow the WHERE clause. 
* this_  -  The implied subject of triplet patterns. Defaults to "?this".

---

Computed properties take:

* data  -  An object representing the query to modify, contains the above properties.
* subject  -  The subject for this object. 
* object
*/
define({
    encodeSearch: function(search, data) {
        // Fill out data
        data.select |= ["?this"]
        data.body |= []
        data.filters |= []
        data.modifiers |= []
        data.this_ |= "?this"
        data.__i = 0

        // Now build the body of the query . . .
        var body = ""
        for (var i = 0; i < search.terms.length; i++) 
            body += search.terms[i].encode(this, data)

        // And place within a SELECT or DESCRIBE query filled out with 
        // data.
        var ret;
        if (data.describe) ret = "DESCRIBE ?" + data.describe
        else if (data.select) ret = "SELECT " + data.select.join(" ")
        else throw "Expected either a describe or select flag in data."

        ret += " WHERE {" + body + data.body.join(". ")
        ret += "FILTER" + data.filters.join(" && ") + "}"
        ret += data.modifiers.join(" ")

        return ret
    },
    encodeTerm: function(term, data) {
        // Check for computed properties
        for (var i = 0; i < term.predicates.length; i++) {
            var pred = term.predicates[i]
            if (this[pred]) {
                // If so, split the term around it...
                var subject = this.Var(data),
                    object = this.Var(data),
                    prefix = {predicates: term.predicates.slice(0, i),
                                object: subject},
                    suffix = {predicates: term.predicates.slice(i+1),
                                object: term.object},
                    ret = ""

                // Things behave a bit differently if this is the first or
                // last property in the path
                if (i == 0) {
                    subject = data.this_
                    prefix = null
                }
                if (i == term.predicates.length - 1) {
                    object = term.object
                    suffix = null
                }

                // AND CALL THIS METHOD
                this[pred](data, subject, object)

                if (prefix) ret += this.encodeTerm(prefix, data)
                if (suffix) {
                    // Suffix has new this_
                    var prevThis_ = data.this_
                    data.this_ = object
                    ret += this.encodeTerm(suffix, data)
                    data.this_ = prevThis_
                }
            }
        }

        // USUAL CASE:
        return data.this_ + " " + term.predicates.join(" / ") + " " + term.object + ". "
    },
    encodeURL: function(url, data) {
        return "<" + url.url + ">"
    },
    encodeLiteral: function(lit, data) {
        return "'" + lit.text + "'^^" + this.encodeURL(lit.type)
    },

    /* TODO Implement "filters" which translates to modifiers. */
})