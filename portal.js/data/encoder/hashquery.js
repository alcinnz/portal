/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
Implements an encoder to translate a search into a compact and readable hashstring, and
a decoder (decodeSearch(text)) from the hashstring to a search. 

Allows users to bookmark searches, and to navigate their search history via the browser buttons. 
Decoder sets flag .inResults to true to indicate the search results should be shown. 

Encodes to and from the format '#v!ns:rdf=http://www.w3.org/1999/rdf-syntax-ns#&rdf:first>rdf:rest="Anable"'.
This is designed so that it's roughly human readable and to have minimal syntactic fluff. Plus
namespaces are critical to keeping it compact, as various prefixes usually repeat. 
*/
define(["data/search", "data/term"], function (Search, Term) {
    return {
        encodeSearch: function(search) {
            var ret = this.inResults ? search.view + "!" : ""

            // Encode namespaces
            var namespaces = search.namespaces()
            for (var ns in namespaces)
                ret += this.encodeTerm({predicates: ["ns:"+ns], object: namespaces[ns]})

            // Encode terms
            for (var i = 0; i < search.terms.length; i++)
                ret += search.terms[i].encode(this)

            return ret.substring(0, this.length - 1) // Clip trailing &
        },
        inResults: false,
        encodeTerm: function(term) {
            return term.predicates.join(">") + "=" + (term.object.encode ?
                term.object.encode(this) : term.object) + "&"
        },
        encodeURL: function(url) {
            return url.url
        },
        encodeLiteral: function(lit) {
            return "'" + lit.text + "'^" + lit.type
        }
        
        // Decoding
        // TODO Consider using a regular expression. 
        decodeSearch: function(text, _close) {
            var search = new Search();
            // This simplistic check requires views to use at most one character in their name. 
            if (text.charAt(1) == "!") {
                search.view = text.charAt(0)
                text = text.substring(2)
                this.inResults = true
            }
            else if (text.charAt(0) == "!") {
                search.view = ""
                text = text.substring(1)
                this.inResults = true
            }
            // else continue

            while (text.length || text.charAt(0) != _close) {
                var ret = this.decodeTerm(text)
                text = ret.remaining
                if (ret.predicates[0].substring(0,3) == "ns:")
                    search._namespaces[ret.predicates[0].substring(3)] = ret.object
                else
                    search.terms.push(new Term(ret.predicates, ret.object))
            }
            return _close ? {search: search, text: text.substring(1)} : search
        },
        decodeTerm: function(text) {
            var eq = text.indexOf("="),
                predicates = text.substring(0, eq)
            text = text.substring(eq+1)

            var object
            if (this[text.charAt(0)]) object = this[text.charAt(0)](text)
            else {
                var and = text.indexOf("&")
                object = text.substring(0, and)
                text = text.substring(and+1)
            }
            return {text: text, predicates: predicates.split(">"), object: object}
        },
        "{": function(text) {
            return decodeSearch(text, "}")
        },
        "\"": function(text, _quote) {
            if (!_quote) _quote = "\""
            var qt = text.indexOf(quote),
                value = text.substring(0, qt),
                type = "quotedString" // TODO Look at the standards, a standard name was specified. 
            text = text.substring(qt+1)

            if (text.charAt(0) == "^") {
                var and = text.indexOf("&")
                type = text.substring(0, and)
                text = text.substring(and)
            }
            if (text.charAt(0) != "&") throw "Expected an ampersand (&)."
            text = text.substring(1)

            return {value: {value: value, type: type}, text: text}
        },
        "'": function(text) {
            return this["\""](text, "'")
        }
    }
})