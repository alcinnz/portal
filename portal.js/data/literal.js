/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

/**
Class to represent RDF literals. They have a type and some text, and 
can be encoded.

This aids encoding of literals differently in the URL as from SPARQL. 
*/
define(function() {
    function Literal(text, type) {
        this.text = text
        this.type = type || "http://www.w3.org/1999/rdf-schema-ns#PlainLiteral"
    }
    Literal.prototype.encode = function(encoder, data) {
        encoder.encodeLiteral(this, data)
    }
    return Literal
})