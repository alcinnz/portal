{
    "lang" : "<?php echo $_SERVER['HTTP_Accept-Language']; ?>",
    "filter" : ["http://kharma.g3labs.org/filters/nospam", "http://kharma.g3labs.org/filters/childsafe"],
    "promote" : [<?php if ($mobile) echo "\"http://kharma.g3labs.org/trust/mobile\"" ?>],
    "bookmarks" : ["http://portal.net/about/contribute.html", /* Current events search */],
    "doodles" : ["http://portal.net/images/local_doodle.svg", "http://portal.net/images/logo.svg"],
    "vocabs" : ["http://www.w3.org/1999/02/22-rdf-syntax-ns#", "http://purl.org/dc/elements/1.1/",
                "http://dublincore.org/documents/2012/06/14/dcmi-terms/", "http://xmlns.com/foaf/0.1/",
                "http://purl.org/ontology/bibo/", "http://purl.org/goodrelations/v1",
                "http://purl.org/vso/ns", "http://purl.org/tio/ns", "http://purl.org/ontology/po/",
                "http://rdfs.org/sioc/types",
                /* Press.Net News Ontology */
                "http://data.press.net/ontology/asset", "http://data.press.net/ontology/classification",
                "http://data.press.net/ontology/stuff", "http://data.press.net/ontology/event",
                "http://data.press.net/ontology/tag", "http://data.press.net/ontology/identifier"
                /* Portal vocabularies (to interface to the SPARQL filter functions) */
                "http://portal.net/addons/date#", "http://portal.net/addons/formulaic#"],
    "x-props" : ["rdf:subject", "rdf:predicate", "rdf:object", "rdf:first", "rdf:rest", 
                "dc:accrualMethod", "dc:accrualPeriodocity", "dc:accrualPolicy", "dc:date",
                "foaf:mbox_sha1sum", "foaf:geekcode", "foaf:dnaChecksum", "foaf:sha1", "foaf:maker", 
                "foaf:plan", "foaf:myersBriggs", "foaf:holdsAccount", "foaf:theme"]
    "datasources" : ["http://portal.net/addons/renaming.rdf", "http://portal.net/addons/interpretors.rdf"],
    "default-prop" : "dc:subject",
    "class-filtering" : false
}