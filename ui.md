=
Portal UI
=

This redesign of the Portal UI is quite similar to the existing one, but with a few changes. 

==
Search box
==

The search box simply resizes to fit all the phrases that make up the search. On the right we have buttons for different views and a search button. 

===
Phrases
===

A phrase is a lightly bordered box containing a label for the property selection and a text entry. Pressing down arrow or clicking the property brings down a property selection which changes the property which the phrase searches. Clicking an arrow on each row allows you to query for all resources with the specified property and value. Various keyboard shortcuts available.

Upon selecting a property, the text entry and/or the logo changes to aid entering the correct property type. SPARQL filters would be available under their own header in the properties dropdown (as are embedded queries) as is a section that allows you to query for additional properties. 

Amongst the widgets would be an autocomplete which uses the same dropdown to search installed resource's comments. 

==
Logo
==

Unlike the previous design, the logo would get involved to frame additional widget UI like calendars and map selection. 

==
Widgets
==

To aid text entry, we support various widgets including:

===
URL autocomplete
===

Enter a URL or query over the localized labels of the installed resources of the appropriate type (typically rdfs:Class).

===
Text
===

Nothing special.

===
Numerical
===

Adds + & - buttons on the right.

===
Time
===

Adds + & - buttons to both sides.

===
Dates
===

Select from the calendar within the logo.

====
Ranges
====

Specialization of date where two dates may be selected and the intermediary highlighted.

===
Map
===

Locates an OpenLayers map in the logo, where the base layer is selected by the user on first use. This map may be used to select points and areas. 

===
Search
===

Embeds a search box in the phrase for exhaustingly complex searches. Doesn't display in the logo as that spot would likely be needed for the embedded phrases (or if it does use the logo, a control would allow users to navigate back). 

===
Sort
===

A checkbox which is visually an arrow pointing up or down. 

==
Footer
==

The footer would have a bookmarks bar on the left, site links on the right, and above that status text. This is intended to link to what services they may expect while still being a pure search engine.

==
Google Comparison
==

Does it live up to user's expectations gained from search engines like Google? Let's see. 

===
Web Search
===

Excedes expectations.

Similar homepage but with a smooth learning curve to excessive power and precision. 

===
Toolbar
===

Desirable.

Simple browser integration won't do, so creating addons would really help Portal.

===
Chrome
===

Not provided.

There's enough web browsers already, although I do have an inriguing architecture designed.

===
Mobile
===

Builtin.

Just as it should be. 

===
YouTube
===

Semi-provided.

No hosting, but you CAN search for videos through this interface. 

===
Image Search
===

Provided (excluding search by image).

You can search for images through this interface. 

===
Picasa
===

Unsupported.

Involves hosting which we cannot provide.

===
Books
===

Supported (given others host the books).

You can search for books through this interface.

===
News
===

Supported given ontology (and we bookmark the appropriate link in the footer).

You can search news stories through the interface, given an ontology, and you can bookmark a search for recent news.

===
Maps
===

Unsupported, limit support may be installed.

By installing GeoSPARQL (given Nada supports it), you can perform sophisticated searches, but we do not perform any mapping. However we do support an OpenLayers widget which asks for a mapping service.

===
Earth
===

Unsupported.

Not Portal's job to present a map.

===
Panoramio
===

Unsupported.

No hosting or mapping provided. 

===
Blog Search
===

Requires ontology.

===
Alerts
===

Supported by a special view.

Uses RSS.

===
Scholar
===

Perfectly adequit already.

===
Gmail
===

Unsupported.

Requires hosting.

===
Docs
===

Unsupported.

Requires hosting.

===
Translate
===

Unsupported.

Translation is a totally different task to searching so the same UI can't suffice. However it can be implemented using sophisticated code atop Nada. 

===
Drive
===

Unsupported.

Requires hosting.

===
Calendar
===

Unsupported.

Requires hosting.

===
Cloud Print
===

Unsupported.

Shares nothing with searching, and can best be implemented by making printers servers (or a computer connected to them). 

===
Blogger
===

Unsupported.

Requires hosting.

===
Google+
===

Semi-supported.

Most actions require hosting, but I can provide crucial search features. 

===
Groups
===

Unsupported.

Requires hosting.

===
Code
===

Semi-supported by installable ontology.

While Portal can get users to projects, the rest can easily be hosted decentrally. 

===
Supported
===

These are all ONE search interface!

* Web Search
* Toolbar
* Chrome
* Mobile
* Image Search
* Books
* Alerts
* Scholar
* Shopping

===
Requires Ontology
===

* News
* Blog Search

===
Semi-supported
===

* YouTube
* Maps
  * Requires GeoSPARQL to be installed
  * Supported by the backend
  * Provides supporting map widget (asks for a data source on first use).
* Google+ ("Social Search")
* Code

===
Unsupported
===

* Chrome (web browser)
* Picasa
* Earth
* Panaramio
* GMail
* Docs
* Drive
* Calendar
* Translate (Backend may provide a key component for this)
* Cloud Print
* Blogger
