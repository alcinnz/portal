=
Advanced Inputs
=

I'm finding really little online to help with the design (even less then my graph/micro-macro search results view). SVG seems to be the tool for the tougher jobs though, and I wouldn't require much of it! Just rotation, rounded boxes, and events.

==
Clock
==

Given simple angle computations, I should be render all the hands & markers. Because it's SVG, I can add event handlers to the hands allowing users to rotate them.

==
Calendar
==

I should be able to implement a calendar simply with a table and by deriving code from jQuery UI (a quick search doesn't bring up a large calendar). Clicks would select a date, or drag would select a range.

==
Map
==

OpenLayers would provide the mapping. SVG would lay out controls on the brim. A query would be used to select the basemap or select additional layers. 

==
Transitions
==

Combine an overlay animation with a fade out and in (or other transition). 

==
Doodles
==

A doodle would need to package a logo, background texture for brim controls, a CSS3 transition, and an overlay for that transition.