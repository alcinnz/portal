= 
Portal backend 
=

Portal is designed as UI to intuitively query the semantic web. That leaves the question of how to actually perform the queries. This file attempts to answer that.

==
What can we do so far?
==

Given we trust each site to be the sole authority on their own pages, then we get full support for linked data queries (with some hotspots primarily around classes). 

Any false assumptions can be fixed with POWDER. 

However classes (roughly supported) and literals (not supported) are likely the most searched upon resources so we need to do better. 

==
Where should data reside?
==

As noted, most data can reside on the queried websites and be queried from there, and additional data should have the opportunity to live there too. 

As for data that can't be readily accessed there, I would give to volunteer servers. 

===
Why Would Someone Volunteer?
===

In order to get users to donate storage space, I will need to give them reason to provide it. Naturally I can provide privacy when using a private "Nada" server. However it is more of a necessity to run a private server when it can use a varient of the Djykstra Algorithm to optimize hops to linked pages. 

Also, if they want it, a local server could perform Seeks personalization and/or use WebID authentication in it's searches. 

That is they run a local server for privacy, hop optimized links, and possibly personalization. 

== 
How Would It Work?
==

There would be 2 or 3 basic components required: a means of mapping unmatchable patterns to internet based sets, a means to reverse simple SPARQL function calls, and possibly GeoSPARQL support.

===
Pattern Matching
===

The basic idea would be to use RDF to index RDF literals and some URLs. Under each I'd store an ordered list as a set. Perhaps use a choice of a trie or AVL tree. 

Can use merge sort to perform ranges. 

===
Reverse SPARQL functions
===

Simply map some SpIN to an ordered list like above. SpIN can be used to infer the rest. 

===
GeoSPARQL
===

This would require an index to be defined and some SpIN to infer GeoSPARQL upon that. 
